This is a specification for running sociocracy on Matrix. Sociocracy is one method of running proposals in a group. The stages of a sociocratic proposal are

    drafting
    sharing
    initial reactions
    clarifications
    test for critical concerns​​​​​​
        if some, suggest amendments
            test for critical concerns on amendments
            if none add the ammendment
         then test for concerns on proposal as a whole
    if no critical concerns the proposal passes
    if lots of critical concerns, can withdraw the proposal.

1. A proposal is a room.

2. The room has type democracy.proposal.sociocracy

3. When a room is created, it is in the pre-draft stage i.e. it has no drafting events.

4. To update the room draft, send a drafting event

Draft Event

event type: democracy.proposal.draft
{
    title: string,
    text: string
}

5. To begin the deliberation stages where people can interact with the proposal, send a share event

Share Event

event type: democracy.proposal.share
{
    title: string,
    text: string
}

6. Add an initial reaction event

Reaction Event

event type: democracy.proposal.reaction
{
    reaction: string
}

e.g. reaction: "this is good, we need this"

7. Request clarification. I've added the ability to highlight part of the proposal that the clarification request is about.

Clarification Request event

event type: democracy.proposal.clarification.request
{
    clarificationRequest: string
    highlightStart: int (optional),
    highlightEnd: int (optional),
}

Clarification Respond event

event type: democracy.proposal.clarification.response
{
    clarificationRequestId: int
    clarificationResponse: string
}

8. Approve proposal. Everyone in the group sends one of these to pass the proposal.

event type: democracy.proposal.approve
{
    comment: string
}

9. Raise critical concern

event type: democracy.proposal.concern.raise

10. Withdraw critical concern

event type: democracy.proposal.concern.withdraw

10. Suggest amendment

event type: democracy.proposal.amendment.suggest

11. Comment on amendment

event type: democracy.proposal.amendment.comment

12. Approve amendment

event type: democracy.proposal.amendment.approve

13. Withdraw amendment

event type: democracy.proposal.amendment.withdraw

14. Withdraw proposal. Proposal moves back to draft stage

event type: democracy.proposal.withdraw