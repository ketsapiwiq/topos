# Matrix vote delegation specifications
aka proxy voting aka liquid democracy

## Abstract

People are invited into a `democracy.proposal` room (and eventually kicked out of it) by a bot (?) that receives and memorizes issued Matrix events of type `democracy.mandates`.

## Structure

### Node Rooms
- A Node Room (e.g. `#McDonaldsXAveRoom`) of type `democracy.node`
    
### Proposal Rooms    
- when a new Event of type `democracy.proposal` is created in a Node Room, the bot creates a new room of type `democracy.proposal` (e.g. `#McDonaldsXAveRoom__proposal981739`)
    - it populates the `democracy.proposal` room according to the members of the Node Room that created the proposal (e.g. `#McDonaldsXAveRoom` members)
    -  the bot applies a Vote Delegation protocol (see below) to add and kick out users of this room, based on whether they are mandated for that proposal.

- In the `#McDonaldsXAveRoom` Node Room:
    - every participant can issue a `democracy.mandate` event specifying:
        - (`principal`: the Matrix User ID of the person delegating their vote)
        - `agent`:   the User ID **(or Node Room ID???)^1** allowed to vote on behalf of the principal
        - `domains` (optional):  the domain(s) for which the mandate is valid. (e.g `domains=['accounting','logistics']`)
        - `proposals` (optional):  the proposal(s) for which the mandate is valid. (e.g `proposals=['proposal981739']`)


## Vote Delegation protocol

### Mandates precedence

1. a user's `mandate` that has a `proposals` property is the strongest and the most narrow kind of mandate, it allows someone to delegate a vote only for a proposal
2. a user's `mandate` that has a `domains` property comes after, it allows someone to delegate a vote for a proposal that belongs to a specific domain (topic)
3. a user's `mandate` with no particular property comes after, it allows someone to delegate a vote in general

4. by default, if no `mandate` property has been set by the user, a node room can have a `mandates` property specifying the default vote delegations that can take place if people have not expressed any kind of mandate
  - this would allow a very powerful system of federation by delegating a vote to another node the node trusts while still allowing usrs to take back their mandate when they want to weigh in on a decision more individually
  - this Node mandate has to be decided democratically via a voting protocol at the node's scale


