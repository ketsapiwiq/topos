type color_theme = Dark | Light

type model =
  { matrix_client: Matrix.client ref
  ; current_color_theme: color_theme
  ; current_locale: string ref }

let init matrix_client =
  {current_color_theme= Dark; current_locale= Locale.init; matrix_client}

type msg =
  | GoTo of Router.route
  | ChangeColorTheme of color_theme
  | ChangeLocale of string
  | ToggleMenu
[@@bs.deriving {accessors}]

let update model = function
  | ChangeColorTheme Light ->
      ({model with current_color_theme= Light}, Tea.Cmd.none)
  | ChangeColorTheme Dark ->
      ({model with current_color_theme= Dark}, Tea.Cmd.none)
  | ChangeLocale lc ->
      model.current_locale := lc ;
      (model, Tea.Cmd.none)
  | GoTo _route ->
      (model, Tea.Cmd.none)
  | ToggleMenu ->
      (* should not happen *)
      (model, Tea.Cmd.none)

let header_link msg route icon label' =
  let open Tea.Html in
  Router.link
    ~props:
      [ Icons.aria_label label'
      ; class'
          "flex flex-row align-center h-full items-center px-5 py-0 \
           hover:bg-plasma-blue"
      ; title label' ]
    msg route
    [Icons.icon ~class':"mr-5" icon; span [] [text label']]

let header_link_simple ?(icon_only = false) msg icon label' =
  let open Tea.Html in
  a
    [ Icons.aria_label label'
    ; class'
        "flex flex-row align-center h-full items-center px-5 py-0 \
         hover:bg-plasma-blue"
    ; title label'
    ; href "#"
    ; onClick msg ]
    [ Icons.icon ~class':"mr-5" icon
    ; span [classList [("hidden", icon_only)]] [text label'] ]

let header_fake_link ?(icon_only = false) icon label' =
  let open Tea.Html in
  a
    [ Icons.aria_label label'
    ; classList
        [ ("icon-only", icon_only)
        ; ( "items-center flex flex-row px-5 py-0 h-full hover:bg-plasma-blue"
          , true ) ]
    ; title label'
    ; href "#" ]
    [Icons.icon ~class':"mr-5" icon; span [] [text label']]

let view model =
  let open Tea.Html in
  let logo =
    li
      [class' "m-0 flex flex-col justify-center relative grow"]
      [ Router.link goTo Index
          ~props:[class' "p-0 flex flex-row hover:bg-plasma-blue"]
          [h1 [class' "uppercase m-0"] [text "Topos"]] ]
  in
  let change_theme_button =
    let msg =
      match model.current_color_theme with
      | Dark ->
          ChangeColorTheme Light
      | Light ->
          ChangeColorTheme Dark
    in
    let label =
      match model.current_color_theme with
      | Dark ->
          T.header_light_mode ()
      | Light ->
          T.header_dark_mode ()
    in
    li
      [ class'
          "items-center flex flex-row h-full px-5 py-2.5 hover:bg-plasma-blue"
      ]
      [header_link_simple msg "moon" label]
  in
  let logout_button =
    li
      [ class'
          "items-center flex flex-row h-full px-5 py-2.5 hover:bg-plasma-blue"
      ]
      [header_link_simple (GoTo Logout) "sign-out" (T.header_logout ())]
  in
  let login_button =
    li
      [class' "m-0 flex flex-col justify-center relative hover:bg-plasma-blue"]
      [header_link goTo Login "sign-in" (T.header_login ())]
  in
  (* let signup_button = *)
  (*   li [] [ Components.rpill_button (GoTo Signup) "sign-in" "Register" ] *)
  (* in *)
  let profile_button =
    li
      [ class'
          "items-center flex flex-row h-full px-5 py-2.5 hover:bg-plasma-blue"
      ]
      [header_link goTo Index "user" (T.header_profile ())]
  in
  let settings_button =
    li
      [ class'
          "items-center flex flex-row h-full px-5 py-2.5 hover:bg-plasma-blue"
      ]
      [header_link goTo Index "settings" (T.header_settings ())]
  in
  let hamburger_button =
    li
      [ class'
          "items-center flex flex-row h-full px-5 py-2.5 hover:bg-plasma-blue"
      ]
      [ header_link_simple ~icon_only:true ToggleMenu "menu"
          (T.header_toggle_menu ()) ]
  in
  let locale_dropdown =
    li
      [class' "m-0 flex flex-col justify-center relative group"]
      [ a
          [ class'
              "items-center flex flex-row px-5 py-0 h-full hover:bg-plasma-blue"
          ; href "#" ]
          [text !(model.current_locale)]
      ; ul
          [ class'
              "dropdown invisible opacity-0 absolute top-14 mt-[-1px] left-0 \
               hidden border border-t-0 hover:visible hover:opacity-100 \
               hover:block group-hover:visible group-hover:opacity-100 \
               group-hover:block group-focus-within:visible \
               group-focus-within:opacity-100 group-focus-within:block \
               bg-cardboard-gray text-charcoal-gray dark:bg-charcoal-gray \
               dark:text-cardboard-gray" ]
          [ li
              [class' "clear-both w-full m-0 w-full"]
              [ a
                  [ class'
                      "items-center flex flex-row h-full px-5 py-2.5 \
                       hover:bg-plasma-blue"
                  ; href "#"
                  ; onClick (ChangeLocale "en") ]
                  [text "en"] ]
          ; li
              [class' "clear-both w-full m-0 w-full"]
              [ a
                  [ class'
                      "items-center flex flex-row h-full px-5 py-2.5 \
                       hover:bg-plasma-blue"
                  ; href "#"
                  ; onClick (ChangeLocale "fr") ]
                  [text "fr"] ] ] ]
  in
  let profile_dropdown inside_links =
    let button_text =
      match Matrix.current_user_name model.matrix_client with
      | Some name ->
          name
      | None ->
          T.header_profile ()
    in
    li
      [class' "m-0 flex flex-col justify-center relative group"]
      (* [ a [ href "#" ] [ text button_text ] *)
      [ header_fake_link "user" button_text
      ; ul
          [ class'
              "dropdown invisible opacity-0 absolute top-14 mt-[-1px] left-0 \
               hidden border border-t-0 hover:visible hover:opacity-100 \
               hover:block group-hover:visible group-hover:opacity-100 \
               group-hover:block group-focus-within:visible \
               group-focus-within:opacity-100 group-focus-within:block \
               bg-cardboard-gray text-charcoal-gray dark:bg-charcoal-gray \
               dark:text-cardboard-gray" ]
          inside_links ]
  in
  let settings_dropdown inside_links =
    (* let () = Js.log (T.header_settings ()) in *)
    li
      [class' "m-0 flex flex-col justify-center relative group"]
      (* [ a [ href "#" ] [ text "settings" ] *)
      [ header_fake_link "options" (T.header_settings ())
      ; ul
          [ class'
              "dropdown invisible opacity-0 absolute top-14 mt-[-1px] right-0 \
               hidden border border-t-0 hover:visible hover:opacity-100 \
               hover:block group-hover:visible group-hover:opacity-100 \
               group-hover:block group-focus-within:visible \
               group-focus-within:opacity-100 group-focus-within:block \
               bg-cardboard-gray text-charcoal-gray dark:bg-charcoal-gray \
               dark:text-cardboard-gray" ]
          inside_links ]
  in
  let items_list =
    if Auth.is_logged_in model.matrix_client then
      (* [ hamburger_button *)
      (* ; logo *)
      (* ; profile_button *)
      (* ; settings_button *)
      (* ; logout_button *)
      (* ; change_theme_button *)
      (* ] *)
      [ hamburger_button
      ; logo
      ; locale_dropdown
      ; profile_dropdown [profile_button; logout_button]
      ; settings_dropdown [settings_button; change_theme_button] ]
    else
      [ hamburger_button
      ; logo
      ; locale_dropdown
      ; login_button (* ; signup_button *)
      ; change_theme_button ]
  in
  header
    [ class'
        "border-b h-14 items-center shrink-0 bg-cardboard-gray \
         text-charcoal-gray dark:bg-charcoal-gray dark:text-cardboard-gray" ]
    [ nav
        [class' "h-full"]
        [ul [class' "flex flex-row h-full items-stretch m-0 p-0"] items_list] ]
