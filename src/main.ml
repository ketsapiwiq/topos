external is_hot : bool = "hot" [@@bs.val] [@@bs.scope "import", "meta"]

external hot_accept : (unit -> unit) -> unit = "accept"
  [@@bs.val] [@@bs.scope "import", "meta", "hot"]

external hot_accept_dep : string -> (string -> unit) -> unit = "accept"
  [@@bs.val] [@@bs.scope "import", "meta", "hot"]

external hot_accept_deps : string array -> (string array -> unit) -> unit
  = "accept"
  [@@bs.val] [@@bs.scope "import", "meta", "hot"]

external hot_dispose : (unit -> unit) -> unit = "dispose"
  [@@bs.val] [@@bs.scope "import", "meta", "hot"]

let container = Web.Document.getElementById "main"

let run () =
  if Config.is_dev then
    if is_hot then
      let shutdown_fun = ref (App.start_hot_debug_app container None) in
      hot_accept_dep "/src/app.ml" (fun _mod ->
          Js.log "accept_dep" ;
          (* let _ = Js.Nullable.bind container (fun c -> c##setAttribute "hidden" "true") in *)
          let model = !shutdown_fun () in
          shutdown_fun :=
            ( [%raw {|_mod.start_hot_debug_app|}]
              :    Web_node.t Js.nullable
                -> App.model option
                -> unit
                -> App.model option )
              container model ;
          () )
    else App.start_debug_app container |. ignore
  else App.start_app container |. ignore

let _ = Js.Global.setTimeout (fun _ -> run ()) 0
