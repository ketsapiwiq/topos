FROM node:17

USER node

RUN mkdir /home/node/app

WORKDIR /home/node/app

COPY --chown=node package-lock.json package.json esy.json esy.lock packages /home/node/app/

RUN npm run setup

EXPOSE 3000

CMD ["npm", "run", "dev"]
