const { FluentParser } = require('fluent-syntax')
const FluentCompiler = require('./compiler2')
const glob = require('glob');
const path = require('path');
const fs = require('fs');

module.exports = class SvgSpriteHtmlWebpackPlugin {
  constructor(options = {}) {
    console.log("test")
    this.svgList = [];
    if (options.includeFiles) {
      this.importFiles(options.includeFiles);
    }
  }

  /**
   * Include files in svg sprite without javascript import
   * @param {string[]} includePaths - list of paths to include
   */
  importFiles(includePaths) {
    const filesToInclude = includePaths
      .reduce((acc, includePath) => {
        const matchPaths = glob.sync(includePath);
        if (matchPaths.length === 0) {
          console.warn(`WARNING : No file match with regex path "${includePath}"`);
        }
        return acc.concat(matchPaths);
      }, [])
      .map((filePath) => {
        const absolutePath = path.resolve(filePath);
        const content = fs.readFileSync(absolutePath, { encoding: 'utf-8' });
        return {
          content,
          filePath: absolutePath,
        };
      });

    filesToInclude.forEach(({ content, filePath }) => {
      const symbolId = path.basename(filePath, '.svg');
      this.handleFile(content, filePath, symbolId);
    });
  }

  /**
   * Handle file imported by loader
   * @param {string} content - svg file content
   * @param {string} path - svg file path
   * @param {string} defaultSymbolId - symbol id to use instead of generate an integer id
   * @return {string} javascript export string with svg symbol id
   */
  handleFile(content, filePath, defaultSymbolId = null) {
    const svgHash = null;
    // search an svg already loaded with the same hash
    const maybeSvgItem = this.svgList.find(item => item.hash === svgHash);
    const isAlreadyLoaded = !!maybeSvgItem;

    const symbolId = null;

    if (!isAlreadyLoaded) {
      const svgItem = {
        id: symbolId,
        hash: svgHash,
        path: filePath,
        content,
      };
      this.pushSvg(svgItem);
    }

    return `export default '#${symbolId}'`;
  }

  /**
   * Add or replace a svg to compile in this.svgList
   * @param {object} svgItem - svg to push in list of svg to compile
   * @param {string} svgItem.id
   * @param {number} svgItem.hash
   * @param {string} svgItem.path
   * @param {string} svgItem.content
   */
  pushSvg(svgItem) {
    // avoid to have 2 svg in the list for the same path
    const listWithoutPreviousItemVersion = this.svgList
      .filter(item => item.path !== svgItem.path);
    this.svgList = [...listWithoutPreviousItemVersion, svgItem];
  }

  /**
   * Function called by webpack during compilation
   * @param {object} compiler - webpack compiler
   */
  apply(compiler) {
    console.log("test")
    // compiler.hooks.beforeRun.tapAsync('FluentBsWebpackPlugin', (compiler, callback) => {
        console.log("test")
        const parser = new FluentParser()
        console.log(this.svgList[0].content)
        const source = parser.parse(this.svgList[0].content)
        const fluent_compiler = new FluentCompiler({})
        const js = fluent_compiler.compile("en", source)
        fs.writeFileSync('./src/t.ml', js)
    // })
  }
};
