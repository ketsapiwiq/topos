FROM node:17 as build-stage

USER node

RUN mkdir /home/node/app

WORKDIR /home/node/app

COPY --chown=node package-lock.json package.json esy.json esy.lock /home/node/app/

COPY --chown=node esy.lock /home/node/app/esy.lock/

COPY --chown=node packages /home/node/app/packages/

RUN npm run setup

COPY --chown=node dune-project bsconfig.json index.html vite.config.js postcss.config.js tailwind.config.js /home/node/app/

COPY --chown=node public /home/node/app/public/

COPY --chown=node src /home/node/app/src/

COPY --chown=node styles /home/node/app/styles/

ARG VITE_API_URL

ARG VITE_MATRIX_URL

ARG VITE_MATRIX_HOMESERVER

ENV VITE_API_URL=$VITE_API_URL

ENV VITE_MATRIX_URL=$VITE_MATRIX_URL

ENV VITE_MATRIX_HOMESERVER=$VITE_MATRIX_HOMESERVER

ENV NODE_ENV=production

RUN npm run build


FROM nginx:1.21

COPY --from=build-stage /home/node/app/dist/ /usr/share/nginx/html

COPY nginx.conf /etc/nginx/conf.d/default.conf
